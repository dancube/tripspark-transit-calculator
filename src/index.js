import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

const root = document.getElementById('transit_calculator_container');

ReactDOM.render(
  <React.StrictMode>
    <App dataset={root.dataset} />
  </React.StrictMode>,
  document.getElementById('transit_calculator_container')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
