# Transit Calculator

This is a calculating tool done in ReactJS to calculate annual savings of a transit company by considering the trips per day, no-show rate, costs per no-show, and % fewer no-shows.

The intended use of this portable tool is by including this in existing pages.

# Building

This app is available already in the [tripspark.com](https://tripspark.com) server. You can just quickly proceed to the next section: How to Use.

If by any chance you need the src files, you'll have to clone and `yarn build` it.

# How to Use

## Recommended

1. Create a `<div id="transit_calculator_container"></div>` and insert it to wherever you would like the calculator to be placed in your code/page.
2. Insert this inside `<head>`:
```
<link rel="manifest" href="https://tripspark.com/assets/js/transit_calculator/build/manifest.json"/>
<link href="https://tripspark.com/assets/js/transit_calculator/build/static/css/main.36b4cf90.chunk.css" rel="stylesheet">
```
3. Insert this at the bottom of your `<body>`:

```
<script>!function(e){function t(t){for(var n,l,a=t[0],i=t[1],c=t[2],p=0,s=[];p<a.length;p++)l=a[p],Object.prototype.hasOwnProperty.call(o,l)&&o[l]&&s.push(o[l][0]),o[l]=0;for(n in i)Object.prototype.hasOwnProperty.call(i,n)&&(e[n]=i[n]);for(f&&f(t);s.length;)s.shift()();return u.push.apply(u,c||[]),r()}function r(){for(var e,t=0;t<u.length;t++){for(var r=u[t],n=!0,a=1;a<r.length;a++){var i=r[a];0!==o[i]&&(n=!1)}n&&(u.splice(t--,1),e=l(l.s=r[0]))}return e}var n={},o={1:0},u=[];function l(t){if(n[t])return n[t].exports;var r=n[t]={i:t,l:!1,exports:{}};return e[t].call(r.exports,r,r.exports,l),r.l=!0,r.exports}l.m=e,l.c=n,l.d=function(e,t,r){l.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},l.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},l.t=function(e,t){if(1&t&&(e=l(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(l.r(r),Object.defineProperty(r,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var n in e)l.d(r,n,function(t){return e[t]}.bind(null,n));return r},l.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return l.d(t,"a",t),t},l.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},l.p="/";var a=this.webpackJsonptransit_calculator=this.webpackJsonptransit_calculator||[],i=a.push.bind(a);a.push=t,a=a.slice();for(var c=0;c<a.length;c++)t(a[c]);var f=i;r()}([])</script>
<script src="https://tripspark.com/assets/js/transit_calculator/build/static/js/2.970f99d1.chunk.js"></script>
<script src="https://tripspark.com/assets/js/transit_calculator/build/static/js/main.54316a9c.chunk.js"></script>
```

## Alternative

**CAUTION: As much as possible please use the method above.**

This alternative is offered in the event that later on changes will be made to the application and references to the old hashed filenames being updated would cause it to break.

1. Create a `<div id="transit_calculator_container"></div>` and insert it to wherever you would like the calculator to be placed in your code/page.
2. Insert this inside `<head>`:
```
<link rel="manifest" href="https://tripspark.com/assets/js/transit_calculator/build/manifest.json"/>
<link href="https://tripspark.com/tc_main_css" rel="stylesheet">
```
3. Insert this at the bottom of your `<body>`:

```
<script>!function(e){function t(t){for(var n,l,a=t[0],i=t[1],c=t[2],p=0,s=[];p<a.length;p++)l=a[p],Object.prototype.hasOwnProperty.call(o,l)&&o[l]&&s.push(o[l][0]),o[l]=0;for(n in i)Object.prototype.hasOwnProperty.call(i,n)&&(e[n]=i[n]);for(f&&f(t);s.length;)s.shift()();return u.push.apply(u,c||[]),r()}function r(){for(var e,t=0;t<u.length;t++){for(var r=u[t],n=!0,a=1;a<r.length;a++){var i=r[a];0!==o[i]&&(n=!1)}n&&(u.splice(t--,1),e=l(l.s=r[0]))}return e}var n={},o={1:0},u=[];function l(t){if(n[t])return n[t].exports;var r=n[t]={i:t,l:!1,exports:{}};return e[t].call(r.exports,r,r.exports,l),r.l=!0,r.exports}l.m=e,l.c=n,l.d=function(e,t,r){l.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},l.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},l.t=function(e,t){if(1&t&&(e=l(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(l.r(r),Object.defineProperty(r,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var n in e)l.d(r,n,function(t){return e[t]}.bind(null,n));return r},l.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return l.d(t,"a",t),t},l.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},l.p="/";var a=this.webpackJsonptransit_calculator=this.webpackJsonptransit_calculator||[],i=a.push.bind(a);a.push=t,a=a.slice();for(var c=0;c<a.length;c++)t(a[c]);var f=i;r()}([])</script>
<script src="https://tripspark.com/tc_chunk1"></script>
<script src="https://tripspark.com/tc_chunk2"></script>
```

# Customizations
You can dynamically change the default values of the text fields by adding html attributes in `<div id="transit_calculator_container"></div>`. Here's an example: 

`<div id="transit_calculator_container" data-trips-per-day="99" data-no-show-rate="99" data-cost-per-no-show="99" data-percent-fewer-no-shows="99"></div>`

You can mix and match the html attributes. They don't have to be all present. You can just provide one attribute if that's the only default value you would like to modify.

E.g.: `<div id="transit_calculator_container" data-trips-per-day="99"></div>`

The calculator has its own default values and it will fall back to those if no corresponding query parameters are present.

Please refer to the full link example above to see which attribute corresponds to what text field in the calculator.

**NOTICE**: The previous iteration had the attributes as `camelCase`. Notice that this time it requires dash-separated, all-lowercase format.

**HINT**: Just look at the calculator and change the label to `data-` plus `dash-separated-all-lowercase-name`. E.g.: Trips Per Day becomes `data-trips-per-day`.

# Notes
- Since the application is loaded in your DOM (and not via iframe), you can then style this app using your own css in the parent page.
- This README is constantly updated every time I make updates to the application (I have to, hashed filenames change).
- The Alternative step is maintained by having htaccess redirects.
- https://github.com/facebook/create-react-app/issues/821
- Annual calculations are based in 252 business days (excludes weekends and US holidays)